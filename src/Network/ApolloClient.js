import { ApolloClient } from 'apollo-client';
import { HttpLink } from 'apollo-link-http';
 import { InMemoryCache } from 'apollo-cache-inmemory';
import { setContext } from 'apollo-link-context';


import { AsyncStorage } from 'react-native';

 
const httpLink = new HttpLink({ uri: 'http://192.168.1.4:3100/graphql' });

const authMiddleware = setContext(async (_, { headers }) => {
  const token = await AsyncStorage.getItem('ACCESS_TOKEN');
  return {
    headers: {
      ...headers,
      authorization: token ? `Bearer ${token}` : '',
    },
  };
});

const cache = new InMemoryCache();


const client = new ApolloClient({
  link:  authMiddleware.concat(httpLink),
  cache
});

export default client ;
