
import { createStackNavigator, createSwitchNavigator, createAppContainer } from 'react-navigation';
import HomeScreen from '../Components/HomeScreen';
import OtherScreen from '../Components/OtherScreen';
import AuthLoadingScreen from '../Components/AuthLoadingScreen';
import SignInScreen from '../Components/SignInScreen';
import Posts from '../Components/Posts';
import Phone from '../Components/Phone';

const AppStack = createStackNavigator({ Home: HomeScreen, Other: OtherScreen , Posts : Posts  , Phone : Phone  });
const AuthStack = createStackNavigator({ SignIn: SignInScreen });


const AppNavigator = createAppContainer(createSwitchNavigator(
    {
      AuthLoading: AuthLoadingScreen,
      App: AppStack,
      Auth: AuthStack,
    },
    {
      initialRouteName: 'AuthLoading',
    }
  ));

export default AppNavigator ;
