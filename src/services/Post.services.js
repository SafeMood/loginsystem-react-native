import  gql   from "graphql-tag";
import {AsyncStorage} from 'react-native';
import client from "../Network/ApolloClient";

export default class PostService {
   
    constructor(props){
        this.props = props
   }
  
    async   createPost(post , media = null ) {
       
      const createPost_mutation = gql`
      mutation createPost($text : String! ) {
          createPost(data: { text : $text }) {
            id
            text
            createdAt
          }
      }
        
      `;
        
        const promise =  await  client.mutate({  mutation : createPost_mutation,variables:    {text : post } })
    
       
         return promise
    }
  
   
  
 
  
  
  
    async AllPosts() {

          const query = gql`
          whoami {
            id
             posts{
              id
              text
              media
              createdAt
             }
           }
        
        `;
 
      const promise =  await  client.query({query}); 

      return promise ;
     
    }

    GetPosts = async ( take,skip) => {
       const query = gql`
       query whoami($take: Float, $skip: Float) {
        whoami {
          id
          posts(options: { take: $take, skip: $skip }) {
            id
            text
            media
            createdAt
          }
        }
      }
       `;

   const promise =  await  client.query({query, variables: {  take , skip  }});
   
   return promise;
    }

    
  
     
   

  }
 
