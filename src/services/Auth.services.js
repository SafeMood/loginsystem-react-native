import  gql   from "graphql-tag";
import {AsyncStorage} from 'react-native';
 
import client from "../Network/ApolloClient";

export default class AuthService {
   
    constructor(props){
        this.props = props
   }
  
    async   login(email, password) {
       
      const query = gql`
          query login($email: String!, $password: String!){
              login(email: $email, password: $password) {
                  accessToken
              }
          }
      `;
            
       const promise = await  client.query({query, variables: {email, password}})
       .then(resp => {
        const {accessToken} = resp.data.login;
        this.onLoginSuccess(accessToken)
      }).catch(error => {
         alert('Sorry invalid Credentials')
      });
  
      
       
     
    
      return promise
    }
  
    onLoginSuccess(accessToken) {
        
      this.persistAuth(accessToken);

        
     }
  
    async persistAuth(accessToken) {
      await  this._storeToken(accessToken);
         this.props.navigation.navigate('App');
         
    }
  
      getAccessToken  = async () => {
        try {
          const token = await AsyncStorage.getItem('ACCESS_TOKEN');
           return  token
        } catch (error) {
          // Error retrieving data
        }
      
    }
  
   async logout() {
      await AsyncStorage.clear();
      this.props.navigation.navigate('Auth');
    }
  
    async amIAuthenticated() {
      //city, country
       return  await  client.query({
        query: gql`
            query whoami {
              whoami {
                id
                fullName
                avatarLocation
              }
            }
        `,
      });
     
    }
  
     
    _storeToken = async (accessToken) => {
      try {
        await AsyncStorage.setItem('ACCESS_TOKEN', accessToken);
      } catch (error) {
        console.log('Token Error')
      }
    };

  }
 
