
import React from 'react';
import Textarea from 'react-native-textarea';
import {
   AsyncStorage,
  Button,
  StatusBar,
  StyleSheet,
  View,
  TextInput,
  TouchableOpacity,
  Image
} from 'react-native';
import PostService from '../services/Post.services';
export default class OtherScreen extends React.Component {

    static navigationOptions = {
      title: 'Make a post',
    };
  constructor(props){
    super(props)
    this.PostService = new PostService();
    this.state = {
      text : ''
    }
  }
    render() {
      return (
        <View style={styles.container}>
           <Textarea
                  containerStyle={styles.textareaContainer}
                  style={styles.textarea}
                  onChangeText={(text) => this.setState({text})}
                  maxLength={120}
                  placeholder={'Feel free to tell a story'}
                  placeholderTextColor={'#c7c7c7'}
                  underlineColorAndroid={'transparent'}
                />
 
           <View style={styles.wrapper}>

            <TouchableOpacity onPress={this._createPost} >
              <Image style={styles.btnSend} source={{uri:"https://img.icons8.com/bubbles/50/000000/sent.png"}}   />
            </TouchableOpacity>

          <Button title="See Your Posts" onPress={this._SeePosts} />
          <StatusBar barStyle="default" />
        </View>
          <Button title="I'm done, sign me out" onPress={this._signOutAsync} />
          <StatusBar barStyle="default" />
        </View>
     
      );
    }
  
    _signOutAsync = async () => {
      await AsyncStorage.clear();
      this.props.navigation.navigate('Auth');
    };
    _SeePosts =   () => {
       this.props.navigation.navigate('Posts');
    };
    _createPost =    () => {
      const promise =    this.PostService.createPost(this.state.text);
      promise.then( (resp) => {
        if(resp.data ){
          alert('Post Created')
        }
        // expected output: "foo"
      });
      promise.catch( (err) => {
       
        alert('Error happened in your connection')
      });
    }
  }
  


const styles = StyleSheet.create({
    container: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
    },
    wrapper : {
      height: 250,
      alignItems: 'center',
      justifyContent: 'center',
    },
    btnSend:{
       width:60,
      height:60,
      marginBottom : 10,
       borderRadius:360,
      alignItems:'center',
      justifyContent:'center',
    },
    inputContainer: {
      borderBottomColor: '#F5FCFF',
      backgroundColor: '#FFFFFF',
      borderRadius:30,
      borderBottomWidth: 1,
      height:40,
      flexDirection: 'row',
      alignItems:'center',
      flex:1,
      marginRight:10,
    },
    textareaContainer: {
      height: 180,
      padding: 5,
      backgroundColor: '#F5FCFF',
    },
    textarea: {
      textAlignVertical: 'top',  // hack android
      height: 170,
      fontSize: 14,
      color: '#333',
    },
  });