import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  AsyncStorage,
  Alert,
  Image
} from 'react-native';
  
 
import AuthService from '../services/Auth.services';
 
   
 
export default class SignInScreen extends React.Component {
    static navigationOptions = {
      title: 'Please sign in',
    };
    constructor(props) {
      super(props);
      this.AuthService = new AuthService(props);
      this.state = {
      email   : 'Sven20@hotmail.com',
      password: '123456',
    }
  }
  
    render() {
      return (
        <View style={styles.container}>
        <View style={styles.inputContainer}>
          <Image style={[styles.icon, styles.inputIcon]} source={{uri: 'https://img.icons8.com/dusk/64/000000/gmail.png'}}/>
          <TextInput style={styles.inputs}
              placeholder="Email"
              keyboardType="email-address"
              onChangeText={(email) => this.setState({email})}
              value={this.state.email}
              underlineColorAndroid='transparent'/>
        </View>
        
        <View style={styles.inputContainer}>
          <Image style={[styles.icon, styles.inputIcon]} source={{uri: 'https://img.icons8.com/dusk/64/000000/private2.png'}}/>
          <TextInput style={styles.inputs}
              placeholder="Password"
              secureTextEntry={true}
              onChangeText={(password) => this.setState({password})}
              value={this.state.password}
              underlineColorAndroid='transparent'/>
        </View>
     
        <TouchableOpacity style={styles.restoreButtonContainer}>
            <Text>Forgot?</Text>
        </TouchableOpacity>

        <TouchableOpacity style={[styles.buttonContainer, styles.loginButton]} onPress={this._signInAsync} >
          <Text style={styles.loginText}>Login</Text>
        </TouchableOpacity>

        <TouchableOpacity style={styles.buttonContainer}>
            <Text>Register</Text>
        </TouchableOpacity>

        <TouchableOpacity style={[styles.buttonContainer, styles.fabookButton]}>
          <View style={styles.socialButtonContent}>
            <Image style={styles.icon} source={{uri: 'https://png.icons8.com/facebook/androidL/40/FFFFFF'}}/>
            <Text style={styles.loginText}>Continue with facebook</Text>
          </View>
        </TouchableOpacity>

        <TouchableOpacity style={[styles.buttonContainer, styles.googleButton]}>
          <View style={styles.socialButtonContent}>
            <Image style={styles.icon} source={{uri: 'https://png.icons8.com/google/androidL/40/FFFFFF'}}/>
            <Text style={styles.loginText}>Sign in with google</Text>
          </View>
        </TouchableOpacity>
      </View>
      );
    }
  
    _signInAsync = async () => {
    
      await this.AuthService.login(this.state.email,this.state.password);
      
    };

 
     onClickListener = (viewId) => {
      // Alert.alert("Alert", "Button pressed "+viewId);
      // this.props.navigation.navigate('App');
        this._signInAsync();
  }
  _logWithFb = () => {
    // Alert.alert("Alert", "Button pressed "+viewId);
     this.props.navigation.navigate('App');
    // this._signInAsync();
}
  

  }

   



  const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#FFFFFF',
    },
    inputContainer: {
        borderBottomColor: '#F5FCFF',
        backgroundColor: '#FFFFFF',
        borderRadius:30,
        borderBottomWidth: 1,
        width:250,
        height:45,
        marginBottom:15,
        flexDirection: 'row',
        alignItems:'center'
    },
    inputs:{
        height:45,
        marginLeft:16,
        borderBottomColor: '#03A9F4',
        flex:1,
    },
    icon:{
      width:30,
      height:30,
    },
    inputIcon:{
      marginLeft:15,
      justifyContent: 'center'
    },
    buttonContainer: {
      height:45,
      flexDirection: 'row',
      justifyContent: 'center',
      alignItems: 'center',
      marginBottom:20,
      width:250,
      borderRadius:30,
    },
    loginButton: {
      backgroundColor: '#3498db',
    },
    fabookButton: {
      backgroundColor: "#3b5998",
    },
    googleButton: {
      backgroundColor: "#ff0000",
    },
    loginText: {
      color: 'white',
    },
    restoreButtonContainer:{
      width:250,
      marginBottom:15,
      alignItems: 'flex-end'
    },
    socialButtonContent:{
      flexDirection: 'row',
      justifyContent: 'center',
      alignItems: 'center', 
    },
    socialIcon:{
      color: "#FFFFFF",
      marginRight:5
    }
  });