import React from 'react';
import {Button,View , StyleSheet } from 'react-native';
import * as Permissions from 'expo-permissions';

 
import {Linking, SMS } from 'expo';

export default class Phone extends React.Component {

  render() {
    return (
      <View style={Styles.container}>
     
           <Button title="phone" onPress={this.btnPhoneClicked} />

           <Button title="SMS" onPress={this.btnSMSClicked}/>

      </View>
    );
  }


  askLKermissionsAsync = async () => {
    await Permissions.askAsync(Permissions.Linking);
  };

  askSMSPermissionsAsync = async () => {
    await Permissions.askAsync(Permissions.SMS);
  };
  
  btnPhoneClicked = async () => {
    this.askLKermissionsAsync();
    let result = await Linking.openURL('tel:20111222');
  };


  btnSMSClicked = async () => {
    //await this.askLKermissionsAsync();
    //let result = await Linking.openURL('sms:0609690005');
    await this.askSMSPermissionsAsync();
    let result = await SMS.sendSMSAsync(['0609690005'], 'السلام عليكم اخوي');
  };
  
 

}

const Styles = StyleSheet.create({

    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#FFF',
    },
    imgcircel: {
        width: 200, 
        height: 200,
        borderRadius: 200/2
    } 
});