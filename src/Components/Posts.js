import React, { Component } from "react";
import { ActivityIndicator, FlatList, Text, View } from "react-native";
import { List, ListItem } from "react-native-elements";
import PostService  from '../services/Post.services';
import PostItem from "./parts/PostItem";
export default class Post extends Component {
  constructor(props) {
    super(props);
    this.PostService = new PostService();
    this.state = {
      data:  [],
      take: 6,
      skip: 0,
      data: [],
     loading: false
    };
  }
   

  componentWillMount() {
    this.fetchData(this.state.take,this.state.skip);
  }

fetchData = async (take,skip) => {
  this.setState({ loading: true });
  const   response  =   await   this.PostService.GetPosts( take,skip);
 

  const posts = await response.data.whoami.posts;
    if(posts){
      this.setState(state => ({
        data: [...state.data, ...posts],
        loading: false
      }));
  }
};

  handleEnd = () => {
     this.setState(state => ( { skip: state.skip +  state.take }),
     () => this.fetchData(this.state.take,this.state.skip));
  };

  render() {
    return (
       <PostItem data={this.state.data} handleEnd={this.handleEnd}  loading={this.state.loading} />
    );
  }
}