import * as ImagePicker from 'expo-image-picker';
import * as Permissions from 'expo-permissions';

import React from 'react';
import {
      StyleSheet,
      Text,
      View,
      Image,
      TouchableOpacity,
       AsyncStorage
    } from 'react-native';
import AuthService from '../services/Auth.services';
 


 export default class HomeScreen extends React.Component {
    static navigationOptions = {
      title: 'Welcome to the app!',
    };
    constructor(props) {
      super(props);
      this.AuthService = new AuthService(props);
      this.data = {};
      this.state = {
      user : {
        fullName : "Default User",
        avatarLocation : "https://bootdey.com/img/Content/avatar/avatar6.png"
      }
    }
  }

  whoami =   async () => {
      const resp =     this.AuthService.amIAuthenticated();
      const result = await resp.then(data => {
        const  {whoami}  = data.data;
            this.setState({
              user : {
                ...this.state.user,
                fullName :  whoami.fullName,
                avatarLocation :  whoami.avatarLocation
              }
      })
      

    }).catch(error => {
      alert('Sorry for that');
    });

 }
    async componentDidMount(){
     await  this.whoami();
    
  }
  
    render() {
      return (
        <View style={styles.container}>
             <View style={styles.container}>
          <View style={styles.header}></View>
          <Image style={styles.avatar} source={{uri: this.state.user.avatarLocation}}/>
          <View style={styles.body}>
            <View style={styles.bodyContent}>
              <Text style={styles.name}>{this.state.user.fullName}</Text>
              <Text style={styles.info}>UX Designer / Mobile developer</Text>
               
              <TouchableOpacity style={styles.buttonContainer} onPress={this._PhoneScreen} >
                <Text>Phone</Text>  
              </TouchableOpacity>  

              <TouchableOpacity style={styles.buttonContainer} onPress={this._showMoreApp} >
                <Text>Show me more of the app</Text>  
              </TouchableOpacity>  
              <TouchableOpacity style={styles.buttonContainer} onPress={this._onTakePic} >
                <Text>Take a Pick</Text>  
              </TouchableOpacity>  
              <TouchableOpacity style={styles.buttonContainer} onPress={this._pickImage} >
                <Text>choose a Pick</Text>  
              </TouchableOpacity>         
              <TouchableOpacity style={styles.buttonContainer} onPress={this._signOutAsync}>
                <Text>Actually, sign me out :)</Text> 
              </TouchableOpacity>
            </View>
       </View>
       </View>
        </View>
      );
    }
  
    _showMoreApp = () => {
       this.props.navigation.navigate('Other');
    };

     
    _PhoneScreen = () => {
      this.props.navigation.navigate('Phone');
   };
  
    _signOutAsync = async () => {
      await AsyncStorage.clear();
      this.props.navigation.navigate('Auth');
    };

    getPermissionAsync = async () => {
      if (Constants.platform.ios) {
        const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
        if (status !== 'granted') {
          alert('Sorry, we need camera roll permissions to make this work!');
        }
      }
    }

    _onTakePic = async () => {
      const {
        cancelled,
        uri,
      } = await  ImagePicker.launchCameraAsync({});
      if (!cancelled) {
        console.log(uri);
        this.setState({ 
          user : {
            ...this.state.user,
            avatarLocation : uri 
          }
         });
      }
    }
  
  
    _pickImage = async () => {
      let result = await ImagePicker.launchImageLibraryAsync({
        mediaTypes: ImagePicker.MediaTypeOptions.Images,
        allowsEditing: true,
        aspect: [4, 3],
      });
  
      console.log(result);
  
      if (!result.cancelled) {
        this.setState({
           user : {
          ...this.state.user,
          avatarLocation : result.uri 
        } });
      }
    };
 
  }

   

   

  const styles = StyleSheet.create({
   
    header:{
      backgroundColor: "#00BFFF",
      height:150,
    },
    avatar: {
      width: 130,
      height: 130,
      borderRadius: 63,
      borderWidth: 4,
      borderColor: "white",
      marginBottom:10,
      alignSelf:'center',
      position: 'absolute',
      marginTop:30
    },
    name:{
      fontSize:22,
      color:"#FFFFFF",
      fontWeight:'600',
    },
    body:{
      marginTop:0,
    },
    bodyContent: {
      flex: 1,
      alignItems: 'center',
     },
    name:{
      fontSize:28,
      color: "#696969",
      fontWeight: "600"
    },
    info:{
      fontSize:16,
      color: "#00BFFF",
      marginTop:10
    },
    description:{
      fontSize:16,
      color: "#696969",
      marginTop:10,
      textAlign: 'center'
    },
    buttonContainer: {
      marginTop:10,
      height:40,
      flexDirection: 'row',
      justifyContent: 'center',
      alignItems: 'center',
      marginBottom:20,
      width:200,
      borderRadius:30,
      backgroundColor: "#00BFFF",
    },
  });
   