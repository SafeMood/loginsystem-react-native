import React from 'react';
import { ApolloProvider } from '@apollo/react-hooks';
import AppNavigator from './src/Navigator/AppNavigator';
import client from './src/Network/ApolloClient';

export default function App() {
  return (
    <ApolloProvider client={client}>
    <AppNavigator screenProps={client} />
    </ApolloProvider>
  );
}

 
